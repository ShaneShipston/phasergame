import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset }) {
        super(game, x, y, asset);

        this.width = 300;
        this.height = 210;
        this.open = false;

        this.animations.add('open');
        this.animations.add('close', [5, 4, 3, 2, 1, 0]);
    }

    update () {

    }

    openAnimation() {
        if (!this.open) {
            this.animations.play('open', 30);
        }

        this.open = true;
    }

    closeAnimation() {
        if (this.open) {
            this.animations.play('close', 30);
        }

        this.open = false;
    }
}
