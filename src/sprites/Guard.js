import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, speed, direction }) {
    super(game, x, y, asset);
    this.width = 90;
    this.height = 180;
    this.anchor.setTo(0.5, 1);

    if (direction == 'left') {
      this.direction = -1;
    }
    else if (direction == 'right') {
      this.direction = 1;
    }
    else {
      this.direction = game.rnd.pick([-1, 1]);
    }

    this.animations.add('walk-right', [11,12,13,14,15,16,17,18,19,20]);
    this.animations.add('walk-left', [32,31,30,29,28,27,26,25,24,23,22]);
    this.animations.add('face-right', [0]);
    this.animations.add('face-left', [1]);

    this.speed = speed;
    this.game.physics.enable(this, Phaser.Physics.ARCADE);
    this.body.collideWorldBounds = true;
    this.body.bounce.set(1, 0);
    this.body.gravity.y = 500;
    this.body.velocity.x = speed * direction;
  }

  update () {

    this.game.physics.arcade.collide(this, this.game.floor);
    this.game.physics.arcade.collide(this, this.game.floor2);

    if (this.body.velocity.x > 0) {
      this.direction = 1;
      this.animations.play('walk-right', 15);
    }
    if (this.body.velocity.x < 0) {
      this.animations.play('walk-left', 15);
      this.direction = -1;
    }

    const spriteAction = game.rnd.integerInRange(1, 200);

    if (spriteAction == 1) {
      this.changeDirection();
    }

    if (spriteAction == 2) {
      this.stop();
    }

    if (spriteAction == 3) {
      this.start();
    }
  }

  changeDirection() {
    this.body.velocity.x *= -1;
  }

  stop() {
    this.body.velocity.x = 0;
    if (this.direction == 1) {
      this.animations.play('face-right', 15);
    } else {
      this.animations.play('face-left', 15);
    }
  }

  start() {
    this.body.velocity.x = this.speed * this.direction;
  }
}
