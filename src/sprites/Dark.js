import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset, width, height }) {
        super(game, x, y, asset);

        this.width = width;
        this.height = height;

        this.game.physics.arcade.enable(this);
        this.body.immovable = true;
    }

    update () {
      if(this.overlap(this.game.character)) {
        this.alpha = 0.3;
      } else {
        this.alpha = 0.9;
      }
    }
}
