import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset }) {
        super(game, x, y, asset);

        this.width = 90;
        this.height = 180;
        this.direction = 1;
        this.speed = 200;
        this.crouchSpeed = 100;

        this.game.physics.arcade.enable(this);
        this.body.collideWorldBounds = true;
        this.body.gravity.y = 500;
        this.crouched = false;
        this.isSearching = false;
        this.isLeaving = false;

        this.animations.add('walk-right', [11,12,13,14,15,16,17,18,19,20]);
        this.animations.add('walk-left', [32,31,30,29,28,27,26,25,24,23,22]);
        this.animations.add('face-right', [0]);
        this.animations.add('face-left', [1]);
        this.animations.add('crouch-right', [0]);
        this.animations.add('crouch-left', [12]);
        this.animations.add('crawl-right', [0,1,2,3,4,5,6,7,8,9,10,11]);
        this.animations.add('crawl-left', [23,22,21,20,19,18,17,16,15,14,13,12]);
        this.animations.add('climb', [0,1,2,3]);
        this.animations.add('search', [2]);

        this.body.setSize(70, this.height / 0.7, 25, 0);
    }

    update () {
        this.body.velocity.x = 0;
        this.game.physics.arcade.collide(this, this.game.floor);
        this.game.physics.arcade.collide(this, this.game.floor2);

        if (!this.alive) {
            return;
        }

        if (this.game.character.isLeaving || this.game.character.isSeaching) {
            this.search();
            return;
        }

        // check if coruched to load new texture file
        if (this.keysDown(Phaser.Keyboard.DOWN)) {
          if (!this.crouched) {
            this.loadTexture('martin-crouch', 0);
            this.crouched = true;
          }
        } else {
          if (this.crouched) {
            this.loadTexture('martin', 0);
            this.crouched = false;
          }
        }

        if (this.keysDown(Phaser.Keyboard.DOWN, Phaser.Keyboard.LEFT)) {
          this.crawlLeft();
        } else if (this.keysDown(Phaser.Keyboard.DOWN, Phaser.Keyboard.RIGHT)) {
          this.crawlRight();
        } else if (this.keysDown(Phaser.Keyboard.LEFT)) {
            this.walkLeft();
        } else if (this.keysDown(Phaser.Keyboard.RIGHT)) {
            this.walkRight();
        } else if (this.keysDown(Phaser.Keyboard.DOWN)) {
          this.crouch();
        } else {
          if (this.direction == -1) {
            this.animations.play('face-left');
          } else {
            this.animations.play('face-right');
          }
        }

        if ((this.body.onFloor() || this.body.touching.down) && this.keysDown(Phaser.Keyboard.SPACEBAR)) {
            this.jump();
        }
    }

    search () {
        this.animations.play('search');
    }

    walkLeft() {
        this.direction = -1;
        this.body.velocity.x = this.speed * this.direction;
        this.animations.play('walk-left', 15);
    }

    walkRight() {
        this.direction = 1;
        this.body.velocity.x = this.speed * this.direction;
        this.animations.play('walk-right', 15);
    }

    jump() {
        this.body.velocity.y = -300;
        this.game.audio.jump.play();
    }

    crouch() {
        if (this.direction == 1) {
          this.animations.play('crouch-right', 15);
        } else {
          this.animations.play('crouch-left', 15);
        }
    }

    crawlRight() {
      this.direction = 1;
      this.body.velocity.x = this.crouchSpeed * this.direction;
      this.animations.play('crawl-right', 15);
    }

    crawlLeft() {
      this.direction = -1;
      this.body.velocity.x = this.crouchSpeed * this.direction;
      this.animations.play('crawl-left', 15);
    }

    keysDown(...keys) {
        const confirmed = keys.filter((key) => {
            return this.game.input.keyboard.isDown(key);
        });

        return keys.length === confirmed.length;
    }
}
