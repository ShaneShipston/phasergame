import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset, width, height }) {
        super(game, x, y, asset);

        this.game.physics.arcade.enable(this);
        this.body.immovable = true;
        this.isOpen = false;
        this.canInteract = true;
        this.animations.add('opening', [0,1,2]);
        this.animations.add('closing', [2,1,0]);
        this.animations.add('open', [2]);
        this.animations.add('close', [0]);
        this.body.setSize(80, this.height, 25, 0);
    }

    update () {
      if (!this.isOpen) {
        this.game.physics.arcade.collide(this, this.game.character);
        this.game.physics.arcade.collide(this, this.game.guards);
      }

      if (this.isOpen) {
        this.animations.play('open', 15);
      } else {
        this.animations.play('close', 15);
      }

      if (this.game.character.overlap(this) && this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
        if (this.isOpen) {
          this.close();
        } else {
          this.open();
        }
      }

      if (this.game.input.keyboard.justReleased(Phaser.Keyboard.UP)) {
        this.canInteract = true;
      }
    }

    open () {
      if (this.canInteract) {
        this.animations.play('opening', 15);
        this.game.audio.doorOpen.play();
        this.isOpen = true;
        this.canInteract = false;
      }
    }

    close () {
      if (!this.game.physics.arcade.intersects(this.body, this.game.character.body) && this.canInteract) {
        this.animations.play('closing', 15);
        this.game.audio.doorClose.play();
        this.isOpen = false;
        this.canInteract = false;
      }
    }
}
