import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset }) {
        super(game, x, y, asset);

        this.game.physics.arcade.enable(this);
        this.body.immovable = true;

        this.body.setSize(382, 357, 206, 121);
    }

    update () {
        if (this.game.character.overlap(this) && !this.game.physics.arcade.intersects(this.game.character.body, this.body) && this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
          this.game.character.isClimbing = true;
        }

        if (!this.game.character.overlap(this)) {
          this.game.character.isClimbing = false;
        }

        if (this.game.physics.arcade.intersects(this.game.character.body, this.body) &&  this.game.character.isClimbing) {
            // this is to compare horizontal offset between the hero and the slope
            var dX = this.game.character.x + this.game.character.width / 2 - this.body.x;

            // we manually modify y property of the hero, no need to use trigonometry as it's a 45 degrees angle
            this.game.character.y = this.body.y + this.body.height - this.game.character.height - dX - 32;

            // and we set gravity to zero
            this.game.character.body.gravity.y = 0;
        } else {
            // if we aren't on the slope, set the gravity to its default value
            this.game.character.body.gravity.y = 500;
        }
    }
}
