import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset, width, height, body_width, body_height, body_x_offset, body_y_offset }) {
        super(game, x, y, asset);

        this.width = width;
        this.height = height;

        this.game.physics.arcade.enable(this);
        this.body.immovable = true;
        this.body.checkCollision.left = false;
	      this.body.checkCollision.right = false;

        this.body.setSize(body_width, body_height, body_x_offset, body_y_offset);
    }

    update () {
      this.game.physics.arcade.collide(this, this.game.character);
      this.game.physics.arcade.collide(this, this.game.floor);
    }
}
