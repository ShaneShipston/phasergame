import Phaser from 'phaser';
import ImageButton from '../ui/imagebutton';

export default class extends Phaser.State {
    create () {
        const menuX = this.game.world.camera.view.width / 2 + this.game.world.camera.view.x;
        const menuY = this.game.world.camera.view.height / 2 + this.game.world.camera.view.y;

        // Intro Music
        this.game.introMusic = this.game.add.audio('title', 1, true);
        this.game.introMusic.play();

        // Background
        const background = this.game.add.sprite(menuX, menuY, 'background');
        background.anchor.setTo(0.5, 0.5);

        // Logo
        const logo = this.game.add.sprite(menuX, -300, 'logo');
        logo.anchor.setTo(0.5, 0.5);

        const tween = this.game.add.tween(logo);
        tween.to({ y: 150 }, 1000, 'Linear', true, 0);

        // Start Button
        this.resumeButton = new ImageButton(this.game, 'startButton');
        this.resumeButton.setPos(menuX, 400);
        this.resumeButton.setCallBack(() => {
            this.state.start('Story');
        });
    }

    update () {
        if (this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            this.state.start('Story');
        }
    }
}
