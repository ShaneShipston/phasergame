/* globals __DEV__ */
import Phaser from 'phaser';
import Character from '../sprites/Character';
import Box from '../sprites/Box';
import Elevator from '../sprites/Elevator';
import Guard from '../sprites/Guard';
import Floor from '../sprites/Floor';
import Background from '../sprites/Background';
import Stairs from '../sprites/Stairs';
import Door from '../sprites/Door';
import ImageButton from '../ui/imagebutton';
import Dark from '../sprites/Dark';

export default class extends Phaser.State {
    init () {}
    preload () {}

    create () {
        this.game.world.setBounds(0, 0, 4976, 720);
        this.game.audio = {
            sfx: 1,
            bg: 1,
            effects: [],
            background: []
        };

        this.canPause = false;
        this.upPressed = false;
        this.elevatorLocked = true;

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.background = this.game.add.group();
        this.foreground = this.game.add.group();

        // Sound Effects
        this.game.audio.jump = this.game.add.audio('jump', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.jump);

        this.game.audio.doorOpen = this.game.add.audio('doorOpen', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.doorOpen);

        this.game.audio.doorClose = this.game.add.audio('doorClose', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.doorClose);

        this.game.audio.papers = this.game.add.audio('papers', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.papers);

        this.game.audio.chime = this.game.add.audio('chime', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.chime);

        this.game.audio.no = this.game.add.audio('no', this.game.audio.sfx, false);
        this.game.audio.effects.push(this.game.audio.no);

        // Level Music
        this.game.audio.lobby = this.game.add.audio('lobbyMusic', this.game.audio.bg, true);
        this.game.audio.background.push(this.game.audio.lobby);

        this.game.audio.lobby.play();

        // Overlays
        this.overlays();
        this.intro();

        this.bg = new Background({
            game: this.game,
            x: 0,
            y: this.world.height - 328 - 32,
            asset: 'lobby-level-1'
        });

        this.background.add(this.bg);

        this.bg = new Background({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'lobby-level-2'
        });

        this.background.add(this.bg);

        this.game.floor = new Floor({
            game: this.game,
            x: 0,
            y: this.world.height - 32,
            asset: 'lobby-floor'
        });

        this.foreground.add(this.game.floor);

        this.game.floor2 = new Floor({
            game: this.game,
            x: 0,
            y: 328,
            asset: 'lobby-floor'
        });

        this.foreground.add(this.game.floor2);

        this.game.character = new Character({
            game: this.game,
            x: 40,
            y: this.world.height - 180 - 32,
            asset: 'martin'
        });

        this.foreground.add(this.game.character);
        this.game.camera.follow(this.game.character);

        this.searchableFileCabinet = new Box({
            game: this.game,
            x: 400,
            y: this.world.height - 200 - 32 - 360,
            asset: 'filecabinet-search',
            width: 175,
            height: 192,
            body_width: 65,
            body_height: 56,
            body_x_offset: 55,
            body_y_offset: 45
        });
        this.background.add(this.searchableFileCabinet);

        this.dark = new Dark({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'dark',
            width: 640,
            height: 360
        });

        this.foreground.add(this.dark);

        this.desk2 = new Box({
            game: this.game,
            x: 1300,
            y: this.world.height - 112 - 32 - 360,
            asset: 'desk-4',
            width: 271,
            height: 117,
            body_width: 271,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 52
        });

        this.foreground.add(this.desk2);

        this.chair1 = new Box({
            game: this.game,
            x: 1570,
            y: this.world.height - 90 - 32 - 360,
            asset: 'chair-1',
            width: 63,
            height: 91,
            body_width: 60,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 90
        });

        this.foreground.add(this.chair1);

        this.chair2 = new Box({
            game: this.game,
            x: 1270,
            y: this.world.height - 90 - 32 - 360,
            asset: 'chair-1',
            width: 63,
            height: 91,
            body_width: 60,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 90
        });

        this.chair2.anchor.setTo(.5,0);
        this.chair2.scale.x *= -1;

        this.foreground.add(this.chair2);

        this.chairs = new Box({
            game: this.game,
            x: 3250,
            y: this.world.height - 98 - 32,
            asset: 'chair-table-chair',
            width: 204,
            height: 82,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 24
        });

        this.background.add(this.chairs);

        this.chairs2 = new Box({
            game: this.game,
            x: 1904,
            y: this.world.height - 98 - 32 - 360,
            asset: 'chair-table-chair',
            width: 204,
            height: 82,
            body_width: 120,
            body_height: 72,
            body_x_offset: 40,
            body_y_offset: 24
        });

        this.background.add(this.chairs2);

        this.ficus1 = new Box({
            game: this.game,
            x: 3450,
            y: this.world.height - 148 - 32,
            asset: 'ficus',
            width: 104,
            height: 158,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });

        this.foreground.add(this.ficus1);

        this.ficus2 = new Box({
            game: this.game,
            x: 3900,
            y: this.world.height - 148 - 32 - 350,
            asset: 'ficus',
            width: 104,
            height: 158,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });

        this.foreground.add(this.ficus2);

        this.ficus3 = new Box({
            game: this.game,
            x: 700,
            y: this.world.height - 148 - 32,
            asset: 'ficus',
            width: 104,
            height: 158,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });

        this.foreground.add(this.ficus3);

        this.ficus4 = new Box({
            game: this.game,
            x: 1300,
            y: this.world.height - 148 - 32,
            asset: 'ficus',
            width: 104,
            height: 158,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });

        this.foreground.add(this.ficus4);

        this.filecabinet1 = new Box({
            game: this.game,
            x: 3990,
            y: this.world.height - 200 - 32 - 360,
            asset: 'filecabinet-6',
            width: 175,
            height: 192,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });

        this.background.add(this.filecabinet1);

        this.filecabinet2 = new Box({
            game: this.game,
            x: 4180,
            y: this.world.height - 155 - 32 - 360,
            asset: 'filecabinet-2',
            width: 60,
            height: 146,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });
        this.background.add(this.filecabinet2);

        this.filecabinet3 = new Box({
            game: this.game,
            x: 100,
            y: this.world.height - 155 - 32 - 360,
            asset: 'filecabinet-4',
            width: 175,
            height: 146,
            body_width: 172,
            body_height: 72,
            body_x_offset: 0,
            body_y_offset: 34
        });
        this.background.add(this.filecabinet3);

        this.stairs = new Stairs({
            game: this.game,
            x: 2750,
            y: this.world.height - 482 - 32,
            asset: 'stairs'
        });

        this.foreground.add(this.stairs);

        this.game.door = new Door({
            game: this.game,
            x: 400,
            y: this.world.height - 362,
            asset: 'door-right'
        });
        this.game.doors = this.game.add.group();
        this.game.doors.add(this.game.door);
        this.foreground.add(this.game.doors);

        this.game.door2 = new Door({
            game: this.game,
            x: 3600,
            y: this.world.height - 362,
            asset: 'door-left'
        });
        this.game.doors.add(this.game.door2);

        this.game.door3 = new Door({
            game: this.game,
            x: 3600,
            y: this.world.height - 360 - 360,
            asset: 'door-left'
        });
        this.game.doors.add(this.game.door3);

        this.game.door4 = new Door({
            game: this.game,
            x: 4250,
            y: this.world.height - 360 - 360,
            asset: 'door-left'
        });
        this.game.doors.add(this.game.door4);

        this.game.door5 = new Door({
            game: this.game,
            x: 1700,
            y: this.world.height - 360 - 360,
            asset: 'door-right'
        });
        this.game.doors.add(this.game.door5);

        this.game.door6 = new Door({
            game: this.game,
            x: 1010,
            y: this.world.height - 360 - 360,
            asset: 'door-right'
        });
        this.game.doors.add(this.game.door6);

        this.game.door7 = new Door({
            game: this.game,
            x: 600,
            y: this.world.height - 360 - 360,
            asset: 'door-right'
        });
        this.game.doors.add(this.game.door7);

        this.elevator = new Elevator({
            game: this.game,
            x: 2300,
            y: this.world.height - 570 - 64,
            asset: 'elevator'
        });

        this.background.add(this.elevator);

        this.game.guard = new Guard({
          game: this.game,
          x: 2700,
          y: this.game.height - 32 - 240,
          asset: 'guard',
          speed: 150,
          direction: 'right'
        });

        this.game.guards = this.game.add.group();
        this.game.guards.add(this.game.guard);

        this.game.guard2 = new Guard({
          game: this.game,
          x: 4500,
          y: this.game.height - 32 - 240,
          asset: 'guard',
          speed: 150,
          direction: 'right'
        });
        this.game.guards.add(this.game.guard2);

        this.game.woman = new Guard({
          game: this.game,
          x: 800,
          y: this.game.height - 32,
          asset: 'woman',
          speed: 150,
          direction: 'right'
        });
        this.game.guards.add(this.game.woman);

        this.game.woman2 = new Guard({
          game: this.game,
          x: 4000,
          y: this.game.height - 32 - 360,
          asset: 'woman',
          speed: 150,
          direction: 'right'
        });

        this.game.guards.add(this.game.woman2);
        this.background.add(this.game.guards);
    }

    update () {
        if (!this.elevatorLocked && this.game.character.overlap(this.elevator)) {
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
                this.game.audio.chime.play();
                this.elevator.openAnimation();
                this.game.character.isLeaving = true;

                this.game.time.events.loop(Phaser.Timer.HALF, () => {
                    this.state.start('ComingSoon');
                    this.game.audio.lobby.stop();
                }, this);
            }

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
                this.elevator.closeAnimation();
            }
        }

        if (this.elevatorLocked && this.game.character.overlap(this.elevator)) {
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
              this.game.audio.no.play();
            }
        }

        this.game.physics.arcade.collide(this.game.character, this.searchableFileCabinet, () => {
            if (!this.upPressed && this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
                // Block multiple search
                this.upPressed = true;

                // wait a second
                this.game.time.events.loop(Phaser.Timer.SECOND, () => {
                    this.upPressed = false;
                    this.game.character.isSeaching = false;
                }, this);

                // Search animation
                this.game.character.isSeaching = true;

                // Play paper audio
                this.game.audio.papers.volume = 2;
                this.game.audio.papers.play();

                // Swap UI
                this.itemCollection.loadTexture('badgeComplete', 0);

                // unlock elevator
                this.elevatorLocked = false;
            }
        }, null, this);

        if (this.canPause && !this.enterPressed && this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            this.pauseMenu();
        }

        if (!this.canPause && !this.enterPressed && this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            this.dialog++;
            this.enterPressed = true;

            this.game.time.events.loop(Phaser.Timer.SECOND, this.resetClickHandle, this);

            if (this.dialog > 4) {
                this.dialogOverlay.destroy();

                this.game.time.events.loop(Phaser.Timer.SECOND, () => {
                    this.canPause = true;
                }, this);
            } else {
                this.dialogOverlay.loadTexture(`dialog${this.dialog}`, 0);
            }
        }
    }

    resetClickHandle () {
        this.enterPressed = false;
    }

    render () {
        if (__DEV__) {
        }
    }

    pauseMenu () {
        this.game.paused = true;

        const menuX = this.game.world.camera.view.width / 2 + this.game.world.camera.view.x;
        const menuY = this.game.world.camera.view.height / 2 + this.game.world.camera.view.y;

        // Background
        const background = this.game.add.sprite(menuX, menuY, 'background');
        background.anchor.setTo(0.5, 0.5);

        const backgroundX = background.width / 2;
        const backgroundY = background.height / 2;

        // Overlay
        const accent = this.game.add.sprite(menuX - backgroundX, menuY - backgroundY, 'accent');

        // Overlay
        const overlay = this.game.add.sprite(menuX, menuY, 'briefcase');
        overlay.anchor.setTo(0.5, 0.5);

        // Resume
        this.resumeButton = new ImageButton(this.game, 'resumeButton');
        this.resumeButton.setPos(menuX, menuY + 10);
        this.resumeButton.setCallBack(() => {
            this.game.paused = false;
            this.resumeButton.destroy();
            this.soundButton.destroy();
            this.musicButton.destroy();
            overlay.destroy();
            background.destroy();
            accent.destroy();
        });

        // Sound
        const soundOn = [2, 2, 2];
        const soundOff = [3, 3, 3];
        const soundStart = this.game.audio.sfx ? soundOn : soundOff;

        this.soundButton = this.game.add.button(menuX + backgroundX - 47, menuY - backgroundY + 43, 'buttons', () => {
            if (this.game.audio.sfx === 1) {
                this.game.audio.sfx = 0;
                this.soundButton.setFrames(...soundOff);

                this.game.audio.effects.forEach((music) => {
                    music.volume = 0;
                });
            } else {
                this.game.audio.sfx = 1;
                this.soundButton.setFrames(...soundOn);

                this.game.audio.effects.forEach((music) => {
                    music.volume = 1;
                });
            }
        }, this, ...soundStart);
        this.soundButton.anchor.setTo(0.5, 0.5);

        // Music
        const musicOn = [0, 0, 0];
        const musicOff = [1, 1, 1];
        const musicStart = this.game.audio.bg ? musicOn : musicOff;

        this.musicButton = this.game.add.button(menuX + backgroundX - 127, menuY - backgroundY + 43, 'buttons', () => {
            if (this.game.audio.bg === 1) {
                this.game.audio.bg = 0;
                this.musicButton.setFrames(...musicOff);

                this.game.audio.background.forEach((music) => {
                    music.volume = 0;
                });
            } else {
                this.game.audio.bg = 1;
                this.musicButton.setFrames(...musicOn);

                this.game.audio.background.forEach((music) => {
                    music.volume = 1;
                });
            }
        }, this, ...musicStart);
        this.musicButton.anchor.setTo(0.5, 0.5);
    }

    intro () {
        this.dialog = 1;
        this.enterPressed = false;

        this.dialogOverlay = this.game.add.sprite(0, 0, 'dialog1');
        this.dialogOverlay.fixedToCamera = true;
    }

    overlays () {
        const cameraWidth = this.game.world.camera.view.width;
        const cameraHeight = this.game.world.camera.view.height;

        this.itemCollection = this.game.add.sprite(cameraWidth - 228, cameraHeight - 125, 'badgeIncomplete');
        this.itemCollection.fixedToCamera = true;
    }
}
