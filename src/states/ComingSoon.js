import Phaser from 'phaser';

export default class extends Phaser.State {
    create () {
        const menuX = this.game.world.camera.view.width / 2 + this.game.world.camera.view.x;
        const menuY = this.game.world.camera.view.height / 2 + this.game.world.camera.view.y;

        this.dialog = 1;
        this.justPressed = false;

        this.overlay = this.game.add.sprite(menuX, menuY, 'comingSoon');
        this.overlay.anchor.setTo(0.5, 0.5);
    }
}
