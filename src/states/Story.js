import Phaser from 'phaser';

export default class extends Phaser.State {
    create () {
        const menuX = this.game.world.camera.view.width / 2 + this.game.world.camera.view.x;
        const menuY = this.game.world.camera.view.height / 2 + this.game.world.camera.view.y;

        this.dialog = 1;
        this.justPressed = false;

        this.overlay = this.game.add.sprite(menuX, menuY, 'overlay1');
        this.overlay.anchor.setTo(0.5, 0.5);
    }

    update () {
        if (!this.justPressed && this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            this.dialog++;
            this.justPressed = true;

            this.game.time.events.loop(Phaser.Timer.SECOND, this.resetClickHandle, this);

            if (this.dialog > 4) {
                this.game.introMusic.stop();
                this.state.start('Game');
            } else {
                this.overlay.loadTexture(`overlay${this.dialog}`, 0);
            }
        }
    }

    resetClickHandle () {
        this.justPressed = false;
    }
}
