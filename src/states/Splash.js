import Phaser from 'phaser';
import { centerGameObjects } from '../utils';

export default class extends Phaser.State {
    init () {}

    preload () {
        const menuX = this.game.world.camera.view.width / 2 + this.game.world.camera.view.x;
        const menuY = this.game.world.camera.view.height / 2 + this.game.world.camera.view.y;

        // Background
        const background = this.game.add.sprite(menuX, menuY, 'background');
        background.anchor.setTo(0.5, 0.5);

        this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg');
        this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar');

        centerGameObjects([this.loaderBg, this.loaderBar]);

        this.load.setPreloadSprite(this.loaderBar);
        //
        // load your assets
        //

        // UI
        this.load.image('accent', 'assets/ui/top-accent.png');
        this.load.image('resumeButton', 'assets/ui/resume_game.png');
        this.load.image('briefcase', 'assets/ui/pause-overlay.png');
        this.load.image('badgeIncomplete', 'assets/ui/ui_badge_not_complete.png');
        this.load.image('badgeComplete', 'assets/ui/ui_badge_complete.png');
        this.load.image('comingSoon', 'assets/ui/end_screen.png');
        this.load.spritesheet('buttons', 'assets/ui/new-buttons.png', 62, 62, 4);

        // Story
        this.load.image('overlay1', 'assets/ui/dialog_1.png');
        this.load.image('overlay2', 'assets/ui/dialog_2.png');
        this.load.image('overlay3', 'assets/ui/dialog_3.png');
        this.load.image('overlay4', 'assets/ui/dialog_4.png');
        this.load.image('dialog1', 'assets/ui/lobby_dialog_1.png');
        this.load.image('dialog2', 'assets/ui/lobby_dialog_2.png');
        this.load.image('dialog3', 'assets/ui/lobby_dialog_3.png');
        this.load.image('dialog4', 'assets/ui/lobby_dialog_4.png');

        // Environment
        this.load.spritesheet('lobby-floor', 'assets/sprites/lobby_level_floor.png', 4976, 64);
        this.load.spritesheet('lobby-level-1', 'assets/sprites/lobby_level_1_wall.png', 4976, 328);
        this.load.spritesheet('lobby-level-2', 'assets/sprites/lobby_level_2_wall.png', 4976, 328);
        this.load.spritesheet('ficus', 'assets/sprites/plant_one_104x158.png', 104, 158);
        this.load.spritesheet('dark', 'assets/sprites/black.png', 1, 1);

        // Characters
        this.load.spritesheet('guard', 'assets/sprites/guard_walk_sprite.png', 128, 256);
        this.load.spritesheet('woman', 'assets/sprites/women_walk_sprite.png', 128, 256);
        this.load.spritesheet('martin', 'assets/sprites/martin.png', 128, 256);
        this.load.spritesheet('martin-crouch', 'assets/sprites/martin_crouch_sprite.png', 256, 256);
        this.load.spritesheet('martin-climb', 'assets/sprites/martin_climb_sprite.png', 640, 256);

        // Interactions
        this.load.spritesheet('door-right', 'assets/sprites/door_sprite_black_open_right_130x360.png', 130, 360);
        this.load.spritesheet('door-left', 'assets/sprites/door_sprite_black_open_left_130x360.png', 130, 360);
        this.load.spritesheet('stairs', 'assets/sprites/escalator_sprite_right_687x482.png', 687, 482);
        this.load.spritesheet('elevator', 'assets/sprites/elevator_sprite.png', 192, 138);
        this.load.spritesheet('chair-1', 'assets/sprites/chair_one_63x91.png', 63, 91);
        this.load.spritesheet('chair-2', 'assets/sprites/chair_two_51_92.png', 51, 92);
        this.load.spritesheet('chair-table-chair', 'assets/sprites/chair_table_chair_204x82.png', 204, 82);
        this.load.spritesheet('desk-1', 'assets/sprites/desk_one_172x106_hitbox_72.png', 172, 106);
        this.load.spritesheet('desk-2', 'assets/sprites/desk_two_256x133_hitbox_72.png', 256, 133);
        this.load.spritesheet('desk-3', 'assets/sprites/desk_three_172x99_hitbox_72.png', 172, 99);
        this.load.spritesheet('desk-4', 'assets/sprites/desk_four_271x117_hitbox_72.png', 271, 117);
        this.load.spritesheet('filecabinet-1', 'assets/sprites/file_1x2_60x96.png', 60, 96);
        this.load.spritesheet('filecabinet-2', 'assets/sprites/file_1x3_60x146.png', 60, 146);
        this.load.spritesheet('filecabinet-3', 'assets/sprites/file_2x3_117x146.png', 117, 146);
        this.load.spritesheet('filecabinet-4', 'assets/sprites/file_3x3_175x146.png', 175, 146);
        this.load.spritesheet('filecabinet-5', 'assets/sprites/file_cabinet_sprite_1x3_58x141.png', 58, 141);
        this.load.spritesheet('filecabinet-6', 'assets/sprites/file_cabinet_sprite_3x4_175x192.png', 175, 192);
        this.load.spritesheet('filecabinet-search', 'assets/sprites/file_cabinet_sprite_search_3x4_175x192.png', 175, 192);

        // Audio
        this.load.audio('title', [
            'assets/audio/title.mp3'
        ]);

        this.load.audio('chime', 'assets/audio/chime.mp3');

        this.load.audio('jump', [
            'assets/audio/jump.mp3',
            'assets/audio/jump.ogg'
        ]);

        this.load.audio('papers', [
            'assets/audio/papers.mp3',
            'assets/audio/papers.ogg'
        ]);

        this.load.audio('doorOpen', [
            'assets/audio/dooropen.mp3',
            'assets/audio/dooropen.ogg'
        ]);

        this.load.audio('doorClose', [
            'assets/audio/door.mp3',
            'assets/audio/door.ogg'
        ]);

        this.load.audio('lobbyMusic', [
            'assets/audio/lobby.mp3',
            'assets/audio/lobby.ogg'
        ]);

        this.load.audio('no', [
            'assets/audio/no.mp3'
        ]);
    }

    create () {
        //this.state.start('Game');
        this.state.start('Title');
    }
}
