import Phaser from 'phaser';

export default class extends Phaser.Group {
    constructor (game, key = "defaultButton") {
        super(game);

        this.key = key;

        this.buttonBack = game.add.sprite(0, 0, this.key);
        this.buttonBack.anchor.set(0.5, 0.5);
        this.add(this.buttonBack);

        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputDown.add(this.onDown, this);
    }
    setPos (x, y) {
        this.x = x;
        this.y = y;
    }
    getBack () {
        return this.buttonBack;
    }
    onDown () {
        this.callback();
    }
    setCallBack (callback) {
        this.callback = callback;
    }
}
