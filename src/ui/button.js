import Phaser from 'phaser';

export default class extends Phaser.Group {
    constructor (game, text = 'button', key = "defaultButton", width = 150, event = null, params = null, size = 18, textColor = '#ffffff') {
        super(game);

        this.key = key;
        this.params = params;
        this.text = text;
        this.event = event;

        this.buttonBack = game.add.sprite(0, 0, this.key);
        this.buttonBack.anchor.set(0.5, 0.5);
        this.buttonBack.width = width;
        this.buttonBack.height = size * 2;
        this.add(this.buttonBack);

        this.textField = game.add.text(0, 0, text);
        this.textField.anchor.set(0.5, 0.5);
        this.textField.fontSize = size;
        this.textField.fill = textColor;
        this.textField.font = 'Arial';
        this.add(this.textField);

        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputUp.add(this.onPressed, this);
        this.buttonBack.events.onInputDown.add(this.onDown, this);
    }
    setPos (x, y) {
        this.x = x;
        this.y = y;
    }
    getBack () {
        return this.buttonBack;
    }
    onPressed () {
        if (this.event) {
            eventDispatcher.dispatch(this.event, this.params);
        }
    }
    onDown () {
        this.callback();
    }
    setCallBack (callback) {
        this.callback = callback;
    }
    setTextSize (size) {
        this.textField.fontSize = size + "px";
    }
    setTextPos (xx, yy) {
        this.textField.x = xx;
        this.textField.y = yy;
    }
    setTextColor (textColor) {
        this.textField.fill = textColor;
    }
    getTextField () {
        return this.textField;
    }
    getText () {
        return this.textField.text;
    }
}
